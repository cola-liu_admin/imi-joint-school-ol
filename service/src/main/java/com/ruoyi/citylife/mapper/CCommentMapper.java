package com.ruoyi.citylife.mapper;

import com.ruoyi.citylife.domain.CComment;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.citylife.dto.CommentDetail;

import java.util.ArrayList;
import java.util.List;

/**
 * 评论Mapper接口
 *
 * @author ruoyi
 * @date 2022-08-17
 */
public interface CCommentMapper extends BaseMapper<CComment> {

    ArrayList<CommentDetail> getPCommentsByAid(String aid);

    Integer delCZan(String cid, String id);

    void insertCCai(String cid, String id);

    Integer delCCai(String cid, String id);

    void insertCZan(String cid, String id);

    Integer countCZan(String cid, String id);

    Integer countCCai(String cid, String id);

    CommentDetail getPCommentsByCid(String cid);

    List<CommentDetail> getChildComments(String cid);
}
