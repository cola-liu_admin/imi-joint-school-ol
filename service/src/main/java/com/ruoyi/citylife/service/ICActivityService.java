package com.ruoyi.citylife.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.citylife.domain.CActivity;
import com.ruoyi.citylife.dto.ActivityDetail;

import java.util.ArrayList;

/**
 * 活动Service接口
 *
 * @author ruoyi
 * @date 2020-11-17
 */
public interface ICActivityService extends IService<CActivity> {
  
  ArrayList<CActivity> getSwiperActivity();
  
  ArrayList<CActivity> getActivityList(Integer pageNum, Integer pageSize);

    ActivityDetail getDetailById(String id);

  ArrayList<CActivity> getHotActivityList(Integer pageNum, Integer pageSize);

    void insertCollections(String aid, String id);

  Integer delCollections(String aid, String id);

  void insertAZan(String aid, String id);

  Integer delAZan(String aid, String id);

  void insertACai(String aid, String id);

  Integer delACai(String aid, String id);

    Integer countCollections(String aid, String id);

  Integer countAppreciate(String aid, String id);

  Integer countTread(String aid, String id);
}
