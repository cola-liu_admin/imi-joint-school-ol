package com.ruoyi.citylife.service.impl;

import com.ruoyi.citylife.dto.CommentDetail;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.citylife.mapper.CCommentMapper;
import com.ruoyi.citylife.domain.CComment;
import com.ruoyi.citylife.service.ICCommentService;

import java.util.ArrayList;
import java.util.List;

/**
 * 评论Service业务层处理
 *
 * @author ruoyi
 * @date 2022-08-17
 */
@Service
public class CCommentServiceImpl extends ServiceImpl<CCommentMapper, CComment> implements ICCommentService {

    @Override
    public ArrayList<CommentDetail> getPCommentsByAid(String aid) {
        return baseMapper.getPCommentsByAid(aid);
    }

    @Override
    public Integer delCZan(String cid, String id) {
        return baseMapper.delCZan(cid, id);
    }

    @Override
    public void insertCCai(String cid, String id) {
        baseMapper.insertCCai(cid, id);
    }

    @Override
    public Integer delCCai(String cid, String id) {
        return baseMapper.delCCai(cid, id);
    }

    @Override
    public void insertCZan(String cid, String id) {
        baseMapper.insertCZan(cid, id);
    }

    @Override
    public Integer countCCai(String cid, String id) {
        return baseMapper.countCCai(cid, id);
    }

    @Override
    public Integer countCZan(String cid, String id) {
        return baseMapper.countCZan(cid, id);
    }

    @Override
    public CommentDetail getPCommentsByCid(String cid) {
        return baseMapper.getPCommentsByCid(cid);
    }

    @Override
    public List<CommentDetail> getChildComments(String cid) {
        return baseMapper.getChildComments(cid);
    }
}
