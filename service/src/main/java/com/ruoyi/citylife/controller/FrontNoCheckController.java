package com.ruoyi.citylife.controller;

import com.ruoyi.citylife.domain.CActivity;
import com.ruoyi.citylife.dto.ActivityDetail;
import com.ruoyi.citylife.dto.CommentDetail;
import com.ruoyi.citylife.service.ICActivityService;
import com.ruoyi.citylife.service.ICCommentService;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.utils.StringUtils;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/*
 *@DESCRIPTION
 *@author Ye
 *@create 2020/11/17 23:03
 */
@RestController
@RequestMapping("citylife/nocheck")
public class FrontNoCheckController {
  @Autowired
  private ICActivityService icActivityService;
  @Autowired
  private ICCommentService icCommentService;

  @GetMapping("getSwiperActivity")
  @Cacheable(value = "getSwiperActivity",key = "'swiper'",cacheManager = "redisExpire")
  public R getSwiperActivity() {
    ArrayList<CActivity> list = icActivityService.getSwiperActivity();
    return R.success("swiperList", list);
  }
  @GetMapping("getActivityList/{pageNum}/{pageSize}")
  @Cacheable(value = "getActivityList",key = "'pageNum_'+#pageNum+'_pageSize'+#pageSize",cacheManager = "redisExpire")
  public R getHotActivityList(@PathVariable Integer pageNum,@PathVariable Integer pageSize) {
    ArrayList<CActivity> list = icActivityService.getHotActivityList(pageNum,pageSize);
    return R.success("activityList", list);
  }
  @GetMapping("getHotActivityList/{pageNum}/{pageSize}")
  @Cacheable(value = "getHotActivityList",key = "'pageNum_'+#pageNum+'_pageSize'+#pageSize",cacheManager = "redisExpire")
  public R getActivityList(@PathVariable Integer pageNum,@PathVariable Integer pageSize) {
    ArrayList<CActivity> list = icActivityService.getActivityList(pageNum,pageSize);
    return R.success("activityList", list);
  }

  @GetMapping("getDetailById/{id}")
  @Cacheable(value = "getDetailById",key = "'id_'+#id",cacheManager = "redisExpire")
  public R getDetailById(@PathVariable String id) {
    CActivity cActivity = icActivityService.getById(id);
    if(StringUtils.isNotNull(cActivity)){
      cActivity.setViews(cActivity.getViews() + 1);
      icActivityService.updateById(cActivity);
      ActivityDetail activityDetail = icActivityService.getDetailById(id);
      return R.success("获取成功", activityDetail);
    }
    return R.error("查询失败");
  }

  @GetMapping("getPCommentsByAid/{aid}")
  //@Cacheable(value = "getPCommentsByAid",key = "'aid_'+#aid",cacheManager = "redisExpire")
  @ApiOperation(value = "根据活动id查询一级评论")
  public R getPCommentsByAid(@PathVariable String aid){

    ArrayList<CommentDetail> list = icCommentService.getPCommentsByAid(aid);
    return R.success("获取成功" , list);
  }
  @GetMapping("getCCommentsByCid/{cid}")
//@Cacheable(value = "getPCommentsByAid",key = "'aid_'+#aid",cacheManager = "redisExpire")
  @ApiOperation(value = "根据活动id查询子级评论")
  public R getCCommentsByCid(@PathVariable String cid){
    CommentDetail parentComment = icCommentService.getPCommentsByCid(cid);
    List<CommentDetail> childComments= icCommentService.getChildComments(cid);
    HashMap<String, Object> map = new HashMap<>();
    map.put("parentComment",parentComment);
    map.put("childComments",childComments);
    return R.success("获取成功" , map);
  }
}
