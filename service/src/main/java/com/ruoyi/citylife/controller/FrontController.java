package com.ruoyi.citylife.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ruoyi.citylife.domain.CActivity;
import com.ruoyi.citylife.domain.CComment;
import com.ruoyi.citylife.domain.CUser;
import com.ruoyi.citylife.service.ICActivityService;
import com.ruoyi.citylife.service.ICCommentService;
import com.ruoyi.citylife.service.ICUserService;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.framework.web.service.WxLoginService;
import com.ruoyi.framework.web.service.WxUser;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/*
 *@DESCRIPTION
 *@author Ye
 *@create 2022/08/18 16:01
 */
@RequestMapping("citylife/front")
@RestController
@Api(tags = "前端控制器")
public class FrontController {
    @Autowired
    private WxLoginService wxLoginService;
    @Autowired
    private ICUserService icUserService;
    @Autowired
    private ICActivityService icActivityService;
    @Autowired
    private ICCommentService icCommentService;
    @ApiOperation(value = "写评论")
    @PostMapping("writeComment")
    public R writeComment(@RequestBody CComment cComment, HttpServletRequest request){
      CUser cUser = getCUerByRequest(request);
      if(StringUtils.isNotNull(cUser) && StringUtils.isNotNull(cComment)){

          CActivity cActivity = icActivityService.getById(cComment.getAid());
          cActivity.setComments(cActivity.getComments() + 1);
          icActivityService.updateById(cActivity);
          if (!"0".equals(cComment.getPid())){
              //代表子评论
              CComment pComment = icCommentService.getById(cComment.getPid());
              pComment.setReplies(pComment.getReplies() + 1);
              icCommentService.updateById(pComment);
          }
          cComment.setUid(cUser.getId());
          icCommentService.save(cComment);
          return R.success("评论成功");

      }
      return R.error("评论出错");

    }

    @ApiOperation(value = "收藏活动")
    @GetMapping("activityCollect/{aid}")
    public R activityCollect(@PathVariable String aid, HttpServletRequest request){
        CUser cUser = getCUerByRequest(request);
        if(StringUtils.isNotNull(cUser)){
            CActivity cActivity = icActivityService.getById(aid);
            if(StringUtils.isNotNull(cActivity)){
                Integer count = icActivityService.countCollections(aid, cUser.getId());
                if(count == 0){
                    cActivity.setCollections(cActivity.getCollections() + 1);
                    icActivityService.updateById(cActivity);
                    icActivityService.insertCollections(aid, cUser.getId());
                    return R.success("收藏成功");
                }
            }

        }
        return R.error("收藏出错");
    }

    @ApiOperation(value = "取消收藏活动")
    @GetMapping("activityCollectCancel/{aid}")
    public R activityCollectCancel(@PathVariable String aid, HttpServletRequest request){
        CUser cUser = getCUerByRequest(request);
        if(StringUtils.isNotNull(cUser)){
            CActivity cActivity = icActivityService.getById(aid);
            if(StringUtils.isNotNull(cActivity)){
                cActivity.setCollections(cActivity.getCollections() - 1);
                icActivityService.updateById(cActivity);
                Integer col = icActivityService.delCollections(aid, cUser.getId());
                if (col == 1) {
                    return R.success("取消收藏成功");
                }
            }

        }
        return R.error("取消收藏出错");
    }


    @ApiOperation(value = "点赞活动")
    @GetMapping("activityAppreciate/{aid}")
    public R activityAppreciate(@PathVariable String aid, HttpServletRequest request){
        CUser cUser = getCUerByRequest(request);
        if(StringUtils.isNotNull(cUser)){
            CActivity cActivity = icActivityService.getById(aid);
            if(StringUtils.isNotNull(cActivity)){
                Integer count = icActivityService.countAppreciate(aid, cUser.getId());
                if(count == 0){
                    //删除点踩记录，返回删除数量
                    Integer cai = icActivityService.delACai(aid, cUser.getId());
                    if(cai == 1){
                        cActivity.setUnlikes(cActivity.getUnlikes() -1);
                    }
                    cActivity.setLikes(cActivity.getLikes() + 1);
                    icActivityService.updateById(cActivity);
                    icActivityService.insertAZan(aid, cUser.getId());
                    return R.success("点赞成功");
                }

            }

        }
        return R.error("点赞出错");
    }

    @ApiOperation(value = "取消点赞活动")
    @GetMapping("activityAppreciateCancel/{aid}")
    public R activityAppreciateCancel(@PathVariable String aid, HttpServletRequest request){
        CUser cUser = getCUerByRequest(request);
        if(StringUtils.isNotNull(cUser)){
            CActivity cActivity = icActivityService.getById(aid);
            if(StringUtils.isNotNull(cActivity)){
                Integer zan = icActivityService.delAZan(aid, cUser.getId());
                if(zan == 1) {
                    cActivity.setLikes(cActivity.getLikes() - 1);
                    icActivityService.updateById(cActivity);
                    return R.success("取消点赞成功");
                }

            }

        }
        return R.error("取消点赞出错");
    }


    @ApiOperation(value = "点踩活动")
    @GetMapping("activityTread/{aid}")
    public R activityTread(@PathVariable String aid, HttpServletRequest request){
        CUser cUser = getCUerByRequest(request);
        if(StringUtils.isNotNull(cUser)){
            CActivity cActivity = icActivityService.getById(aid);
            if(StringUtils.isNotNull(cActivity)){
                Integer count = icActivityService.countTread(aid, cUser.getId());
                if(count == 0) {
                    //删除点赞记录，返回删除数量
                    Integer zan = icActivityService.delAZan(aid, cUser.getId());
                    if(zan == 1){
                        cActivity.setLikes(cActivity.getLikes() -1);
                    }
                    cActivity.setUnlikes(cActivity.getUnlikes() + 1);
                    icActivityService.updateById(cActivity);
                    icActivityService.insertACai(aid, cUser.getId());
                    return R.success("点踩成功");
                }

            }

        }
        return R.error("点踩出错");
    }

    @ApiOperation(value = "取消点踩活动")
    @GetMapping("activityTreadCancel/{aid}")
    public R activityTreadCancel(@PathVariable String aid, HttpServletRequest request){
        CUser cUser = getCUerByRequest(request);
        if(StringUtils.isNotNull(cUser)){
            CActivity cActivity = icActivityService.getById(aid);
            if(StringUtils.isNotNull(cActivity)){
                Integer cai = icActivityService.delACai(aid, cUser.getId());
                if(cai == 1) {
                    cActivity.setUnlikes(cActivity.getUnlikes() - 1);
                    icActivityService.updateById(cActivity);
                    return R.success("取消点踩成功");
                }

            }

        }
        return R.error("取消点踩出错");
    }


    @ApiOperation(value = "点赞评论")
    @GetMapping("commentAppreciate/{cid}")
    public R commentAppreciate(@PathVariable String cid, HttpServletRequest request){
        CUser cUser = getCUerByRequest(request);
        if(StringUtils.isNotNull(cUser)){
            CComment cComment = icCommentService.getById(cid);
            if(StringUtils.isNotNull(cComment)){
                Integer count = icCommentService.countCZan(cid, cUser.getId());
                if(count == 0) {
                    //删除点踩记录，返回删除数量
                    Integer cai = icCommentService.delCCai(cid, cUser.getId());
                    if(cai == 1){
                        cComment.setUnlikes(cComment.getUnlikes() -1);
                    }
                    cComment.setLikes(cComment.getLikes() + 1);
                    icCommentService.updateById(cComment);
                    icCommentService.insertCZan(cid, cUser.getId());
                    return R.success("点赞成功");
                }

            }

        }
        return R.error("点赞出错");
    }

    @ApiOperation(value = "取消点赞评论")
    @GetMapping("commentAppreciateCancel/{cid}")
    public R commentAppreciateCancel(@PathVariable String cid, HttpServletRequest request){
        CUser cUser = getCUerByRequest(request);
        if(StringUtils.isNotNull(cUser)){
            CComment cComment = icCommentService.getById(cid);
            if(StringUtils.isNotNull(cComment)){

                Integer zan = icCommentService.delCZan(cid, cUser.getId());
                if(zan == 1) {
                    cComment.setLikes(cComment.getLikes() - 1);
                    icCommentService.updateById(cComment);
                    return R.success("取消点赞成功");
                }

            }

        }
        return R.error("取消点赞出错");
    }


    @ApiOperation(value = "点踩评论")
    @GetMapping("commentTread/{cid}")
    public R commentTread(@PathVariable String cid, HttpServletRequest request){
        CUser cUser = getCUerByRequest(request);
        if(StringUtils.isNotNull(cUser)){
            CComment cComment = icCommentService.getById(cid);
            if(StringUtils.isNotNull(cComment)){
                Integer count = icCommentService.countCCai(cid, cUser.getId());
                if(count == 0) {
                    //删除点赞记录，返回删除数量
                    Integer zan = icCommentService.delCZan(cid, cUser.getId());
                    if(zan == 1){
                        cComment.setLikes(cComment.getLikes() -1);
                    }
                    cComment.setUnlikes(cComment.getUnlikes() + 1);
                    icCommentService.updateById(cComment);
                    icCommentService.insertCCai(cid, cUser.getId());
                    return R.success("点踩成功");
                }

            }

        }
        return R.error("点踩出错");
    }

    @ApiOperation(value = "取消点踩评论")
    @GetMapping("commentTreadCancel/{cid}")
    public R commentTreadCancel(@PathVariable String cid, HttpServletRequest request){
        CUser cUser = getCUerByRequest(request);
        if(StringUtils.isNotNull(cUser)){
            CComment cComment = icCommentService.getById(cid);
            if(StringUtils.isNotNull(cComment)){
                Integer cai = icCommentService.delCCai(cid, cUser.getId());
                if(cai == 1) {
                    cComment.setUnlikes(cComment.getUnlikes() - 1);
                    icCommentService.updateById(cComment);
                    return R.success("取消点踩成功");
                }

            }

        }
        return R.error("取消点踩出错");
    }

    @ApiOperation(value = "获取活动当前用户的状态")
    @GetMapping("getActivityUserStatus/{aid}")
    public R getActivityUserStatus(@PathVariable String aid, HttpServletRequest request) {
        CUser cUser = getCUerByRequest(request);
        if(StringUtils.isNotNull(cUser)) {
            //判断aid对应的活动是否存在
            CActivity cActivity = icActivityService.getById(aid);
            if (StringUtils.isNotNull(cActivity)) {
                HashMap<String, Object> map = new HashMap<>();
                Integer collection = icActivityService.countCollections(aid, cUser.getId());
                Integer appreciate = icActivityService.countAppreciate(aid, cUser.getId());
                Integer tread = icActivityService.countTread(aid, cUser.getId());
                QueryWrapper<CComment> wrapper = new QueryWrapper<>();
                wrapper.eq("aid", aid)
                .orderByDesc("id");
                List<CComment> cCommentList = icCommentService.list(wrapper);
                ArrayList<Integer> commentAppreciate = new ArrayList<>();
                ArrayList<Integer> commentTread = new ArrayList<>();
                for (CComment cComment : cCommentList) {
                    String cid = cComment.getId();
                    Integer cZan = icCommentService.countCZan(cid, cUser.getId());
                    Integer cCai = icCommentService.countCCai(cid, cUser.getId());
                    commentAppreciate.add(cZan);
                    commentTread.add(cCai);

                }
                map.put("collection", collection);
                map.put("appreciate", appreciate);
                map.put("tread", tread);
                map.put("commentAppreciate", commentAppreciate);
                map.put ("commentTread", commentTread);



                return R.success("获取成功", map);
            }
        }
        return R.error("获取失败");
    }



    private CUser getCUerByRequest(HttpServletRequest request) {
        String token = wxLoginService.getToken(request);
        WxUser wxUser = wxLoginService.getWxUser(token);
        String openid = wxUser.getOpenid();
        QueryWrapper<CUser>wrapper = new QueryWrapper<>();
        wrapper.eq("openid" , openid);
        return icUserService.getOne(wrapper);
    }
}
