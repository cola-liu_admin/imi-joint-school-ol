package com.ruoyi.citylife.dto;

import com.ruoyi.citylife.domain.CActivity;
import lombok.Data;

/**
 * 活动详情
 *
 * @author Ye
 * @date 2022-08-12
 */
@Data
public class ActivityDetail extends CActivity {
    // 昵称
    private String nickname;
    // 性别
    private Boolean sex;
    // 头像
    private String avatar;
    // 签名
    private String sign;
    // 用户所在城市
    private String city;
    // 秘籍
    private Integer cheats;
}
