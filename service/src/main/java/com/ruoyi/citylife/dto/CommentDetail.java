package com.ruoyi.citylife.dto;

import com.ruoyi.citylife.domain.CComment;
import lombok.Data;

/**
 * 评论返回对象封装
 *
 * @author Ye
 * @date 2022-08-19
 */
@Data
public class CommentDetail extends CComment {
    //昵称
    private String nickname;
    //头像
    private String avatar;
}
