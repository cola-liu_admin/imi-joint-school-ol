// /common/http.api.js

// 如果没有通过拦截器配置域名的话，可以在这里写上完整的URL(加上域名部分)
// let hotSearchUrl = '/ebapi/store_api/hot_search';
// let indexUrl = '/ebapi/public_api/index';

// 此处第二个参数vm，就是我们在页面使用的this，你可以通过vm获取vuex等操作，更多内容详见uView对拦截器的介绍部分：
// https://uviewui.com/js/http.html#%E4%BD%95%E8%B0%93%E8%AF%B7%E6%B1%82%E6%8B%A6%E6%88%AA%EF%BC%9F
const install = (Vue, vm) => {
	// 登录
	let login = (params = {}) => vm.$u.get(`/wx/user/${params.appid}/login`, params);
	// 授权
	let auth = (params = {}) => vm.$u.post(`/wx/user/${params.appid}/postAuth`, params);
	// 获取轮播图
	let getSwiperActivity = () => vm.$u.get(`/citylife/nocheck/getSwiperActivity`);
	// 获取活动列表
	let getActivityList = (params = {}) => vm.$u.get(
		`/citylife/nocheck/getActivityList/${params.pageNum}/${params.pageSize}`);
	// 获取活动详情
	let getDetailById = (params = {}) => vm.$u.get(
		`/citylife/nocheck/getDetailById/${params.id}/`);
	// 获取热门活动列表
	let getHotActivityList = (params = {}) => vm.$u.get(
		`/citylife/nocheck/getHotActivityList/${params.pageNum}/${params.pageSize}`);
	// 评论
	let writeComment = (params = {}) => vm.$u.post(`/citylife/front/writeComment`, params);

	// 获取评论
	let getPCommentsByAid = (params = {}) => vm.$u.get(
		`/citylife/nocheck/getPCommentsByAid/${params.aid}`, params);

	// 收藏活动
	let activityCollect = (params = {}) => vm.$u.get(
		`/citylife/front/activityCollect/${params.aid}`, params);

	// 取消收藏活动
	let activityCollectCancel = (params = {}) => vm.$u.get(
		`/citylife/front/activityCollectCancel/${params.aid}`, params);

	// 活动点赞
	let activityAppreciate = (params = {}) => vm.$u.get(
		`/citylife/front/activityAppreciate/${params.aid}`, params);

	// 取消活动点赞
	let activityAppreciateCancel = (params = {}) => vm.$u.get(
		`/citylife/front/activityAppreciateCancel/${params.aid}`, params);

	// 活动点踩
	let activityTread = (params = {}) => vm.$u.get(
		`/citylife/front/activityTread/${params.aid}`, params);

	// 取消活动点踩
	let activityTreadCancel = (params = {}) => vm.$u.get(
		`/citylife/front/activityTreadCancel/${params.aid}`, params);

	// 评论点赞
	let commentAppreciate = (params = {}) => vm.$u.get(
		`/citylife/front/commentAppreciate/${params.cid}`, params);

	// 取消评论点赞
	let commentAppreciateCancel = (params = {}) => vm.$u.get(
		`/citylife/front/commentAppreciateCancel/${params.cid}`, params);

	// 评论点踩
	let commentTread = (params = {}) => vm.$u.get(
		`/citylife/front/commentTread/${params.cid}`, params);

	// 取消评论点踩
	let commentTreadCancel = (params = {}) => vm.$u.get(
		`/citylife/front/commentTreadCancel/${params.cid}`, params);

	// 获取活动当前用户相关状态
	let getActivityUserStatus = (params = {}) => vm.$u.get(
		`/citylife/front/getActivityUserStatus/${params.aid}`, params);
		
	// 获取子集评论
	let getCCommentsByCid = (params = {}) => vm.$u.get(`/citylife/nocheck/getCCommentsByCid/${params.cid}`, params);

	// 将各个定义的接口名称，统一放进对象挂载到vm.$u.api(因为vm就是this，也即this.$u.api)下
	vm.$u.api = {
		login,
		auth,
		getSwiperActivity,
		getActivityList,
		getDetailById,
		getHotActivityList,
		writeComment,
		getPCommentsByAid,
		activityCollect,
		activityCollectCancel,
		activityAppreciate,
		activityAppreciateCancel,
		activityTread,
		activityTreadCancel,
		commentAppreciate,
		commentAppreciateCancel,
		commentTread,
		commentTreadCancel,
		getActivityUserStatus,
		getCCommentsByCid

	};
}

export default {
	install
}
